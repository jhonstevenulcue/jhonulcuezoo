package com.zoo.reto.rest;

import com.zoo.reto.domain.LocationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zoo.reto.service.ILocationTypeService;

@RestController
@RequestMapping("/api")
public class LocationTypeResource {

    @Autowired
    ILocationTypeService locationTypeService;

    @PostMapping("/location-type")
    public LocationType create(@RequestBody LocationType locationType){
        return locationTypeService.create(locationType);
    }

    @GetMapping("/location-type")
    public Iterable<LocationType> read(){
        return locationTypeService.read();
    }
}
