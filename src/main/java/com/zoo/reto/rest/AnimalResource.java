package com.zoo.reto.rest;

import com.zoo.reto.domain.Animal;
import org.hibernate.boot.model.source.spi.IdentifierSourceNonAggregatedComposite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.zoo.reto.repository.dto.AnimalNameGender;
import com.zoo.reto.service.IAnimalService;

@RestController
@RequestMapping("/api/animal")
public class AnimalResource {

    @Autowired
    IAnimalService animalService;

    @PostMapping("")
    public ResponseEntity create(@RequestBody Animal animal){
        return animalService.create(animal);
    }

    @GetMapping("")
    public Iterable<Animal> read(){
        return animalService.read();
    }

    @GetMapping("/count")
    public Integer countAnimals(){ return animalService.quantityAnimals(); }

    @GetMapping("/name-gender")
    public Iterable<AnimalNameGender> getAnimalNameGender() { return animalService.getAnimalNameGender(); }

    @GetMapping("/animal/count-gender")
    public Integer countGender(){ return animalService.quantityGender();}

}
