package com.zoo.reto.rest;

import com.zoo.reto.domain.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zoo.reto.service.ILocationService;

@RestController
@RequestMapping("/api/location")
public class LocationResource {

    @Autowired
    ILocationService locationService;

    @PostMapping("")
    public Location create(@RequestBody Location location){
        return locationService.create(location);
    }

    @GetMapping("")
    public Iterable<Location> read(){
        return locationService.read();
    }
}
