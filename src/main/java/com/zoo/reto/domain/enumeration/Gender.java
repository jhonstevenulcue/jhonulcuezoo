package com.zoo.reto.domain.enumeration;

public enum Gender {
    MALE, FEMALE
}
