package com.zoo.reto.domain;

import com.zoo.reto.domain.enumeration.Gender;

import javax.persistence.*;


@Entity

public class Animal {

    @Id
    private String code;
    private String name;
    private String race;

    public Animal() {
    }

    public Animal(String code, String name, String race, Gender gender, Location location) {
        this.code = code;
        this.name = name;
        this.race = race;
        this.gender = gender;
        this.location = location;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ManyToOne
    private Location location;


}
