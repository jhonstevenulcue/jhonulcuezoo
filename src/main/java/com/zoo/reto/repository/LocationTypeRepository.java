package com.zoo.reto.repository;

import com.zoo.reto.domain.Location;
import com.zoo.reto.domain.LocationType;
import org.springframework.data.repository.CrudRepository;

public interface LocationTypeRepository extends CrudRepository<LocationType, Integer> {
}
