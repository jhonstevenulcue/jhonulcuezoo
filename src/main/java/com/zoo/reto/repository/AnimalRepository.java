package com.zoo.reto.repository;

import com.zoo.reto.domain.Animal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.zoo.reto.repository.dto.AnimalNameGender;

public interface AnimalRepository extends CrudRepository<Animal, String> {

    @Query(value = "select count(animal) from Animal animal")
    Integer quantityAnimals();

    @Query(value = "select animal.name as name, animal.gender as gender from Animal animal")
    Iterable<AnimalNameGender> findAnimalNameGender();

    @Query(value = "select count(animal.gender) from Animal animal")
    Integer quantityGender();

}
