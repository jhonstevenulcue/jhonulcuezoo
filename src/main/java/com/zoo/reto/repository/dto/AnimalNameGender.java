package com.zoo.reto.repository.dto;

public interface AnimalNameGender {

    public String getName();
    public String getGender();
}
