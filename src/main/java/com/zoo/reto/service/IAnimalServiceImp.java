package com.zoo.reto.service;

import com.zoo.reto.domain.Animal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.zoo.reto.repository.AnimalRepository;
import com.zoo.reto.repository.dto.AnimalNameGender;

import java.util.Optional;

@Service
public class IAnimalServiceImp implements IAnimalService {

    @Autowired
    private AnimalRepository animalRepository;

    @Override
    public ResponseEntity create(Animal animal) {

        if(animalRepository.findById(animal.getCode()).isPresent()){
            return new ResponseEntity("This code is used", HttpStatus.BAD_REQUEST);

        } else {
            return new ResponseEntity(animalRepository.save(animal),HttpStatus.OK);
        }
    }

    @Override
    public Iterable<Animal> read() { return animalRepository.findAll(); }

    @Override
    public Animal update(Animal animal) { return animalRepository.save(animal); }

    @Override
    public Optional<Animal> getById(String code) { return animalRepository.findById(code); }

    @Override
    public Integer quantityAnimals() { return animalRepository.quantityAnimals(); }

    @Override
    public Iterable<AnimalNameGender> getAnimalNameGender() { return animalRepository.findAnimalNameGender(); }

    @Override
    public Integer quantityGender() { return animalRepository.quantityGender(); }
}
