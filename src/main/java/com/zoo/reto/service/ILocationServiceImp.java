package com.zoo.reto.service;

import com.zoo.reto.domain.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zoo.reto.repository.LocationRepository;

@Service
public class ILocationServiceImp implements ILocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public Location create(Location location) { return locationRepository.save(location); }

    @Override
    public Iterable<Location> read() { return locationRepository.findAll(); }
}
