package com.zoo.reto.service;

import com.zoo.reto.domain.LocationType;

public interface ILocationTypeService {

    public LocationType create(LocationType locationType);

    public Iterable<LocationType> read();
}
