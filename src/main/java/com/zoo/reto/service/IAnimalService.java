package com.zoo.reto.service;

import com.zoo.reto.domain.Animal;
import org.springframework.http.ResponseEntity;
import com.zoo.reto.repository.dto.AnimalNameGender;

import java.util.Optional;

public interface IAnimalService {

    public ResponseEntity create(Animal animal);

    public Iterable<Animal> read();

    public Animal update (Animal animal);

    public Optional<Animal> getById(String code);

    public Integer quantityAnimals();

    public Iterable<AnimalNameGender> getAnimalNameGender();

    public Integer quantityGender();

   // public Iterable<>

}
