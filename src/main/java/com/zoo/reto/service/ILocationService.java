package com.zoo.reto.service;

import com.zoo.reto.domain.Location;

public interface ILocationService {

    public Location create(Location location);

    public Iterable<Location> read();
}
