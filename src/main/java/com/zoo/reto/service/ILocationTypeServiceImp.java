package com.zoo.reto.service;

import com.zoo.reto.domain.LocationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zoo.reto.repository.LocationTypeRepository;

@Service
public class ILocationTypeServiceImp implements ILocationTypeService {

    @Autowired
    private LocationTypeRepository locationTypeRepository;

    @Override
    public LocationType create(LocationType locationType) {
        return locationTypeRepository.save(locationType);
    }

    @Override
    public Iterable<LocationType> read() { return locationTypeRepository.findAll(); }
}
